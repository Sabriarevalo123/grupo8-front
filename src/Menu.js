import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import axios from "axios";

const Menu = () => {
  const [imageBase64, setImageBase64] = useState("");
  const [image, setImage] = useState("");

  const handleClick = async () => {
    try {
      const response = await axios.post(
        "http://18.217.42.191/predecir/",

        [
          85, 59, 26, 72, 3, 85, 81, 26, 26, 58, 81, 81, 59, 72, 84, 88, 59, 59,
          72, 72, 3, 85, 81, 26, 88, 58, 81, 88, 85, 84, 58, 58, 26, 84, 26, 84,
          72, 85, 85, 84, 3, 58, 84, 84, 58, 85, 3, 84, 3, 58, 88, 72, 3, 81,
          26, 59, 88, 81, 85, 58, 58, 88, 81, 58, 58, 88, 85, 85, 84, 26, 88,
          59, 59, 72, 58, 3, 85, 72, 3, 85, 59, 81, 58, 59, 84, 3, 58, 26, 81,
          58, 85, 72, 3, 59, 85, 84, 88, 81, 58, 58, 3, 88, 84, 58, 58, 85, 59,
          26, 59, 88, 3, 58, 81, 88, 72, 81, 72, 85, 26, 59, 81, 72, 58, 88, 81,
          26, 59, 59, 81, 58, 58, 72, 85, 85, 58, 58, 58, 26, 58, 58, 26, 88, 3,
          85, 88, 58, 26, 84, 85, 72, 3, 88, 84, 84, 85, 58, 72, 3, 59, 84, 58,
          88, 26, 58, 3, 58, 58, 26, 81, 84, 58, 88, 59, 59, 85, 84, 81, 3, 26,
          88, 58, 81, 58, 84, 59, 26, 85, 72, 72, 3, 58, 59, 72, 81, 3, 72, 26,
          58, 59, 58,
        ]
      );
      const imageData = "data:image/jpeg;base64," + response.data;
      setImageBase64(imageData);
      console.log(imageData);

      console.log(response.data);
    } catch (error) {
      // Maneja errores si la solicitud falla
      console.error("Error al llamar a la API:", error);
    }
  };
  const handleClick2 = async () => {
    try {
      const response = await axios.post(
        "http://18.217.42.191/probabilidad/",
        [10, 26, 45, 56, 68, 70, 89]
      );
      const image = "data:image/jpeg;base64," + response.data;
      setImage(image);
      console.log(image);

      console.log(response.data);
    } catch (error) {
      // Maneja errores si la solicitud falla
      console.error("Error al llamar a la API:", error);
    }
  };

  return (
    <Grid container spacing={12} justifyContent="center" alignItems="center">
      <Grid item md={2}>
        {" "}
      </Grid>
      <Grid item md={4}>
        <h2>Predecir</h2>
      </Grid>
      <Grid item md={2}>
        {" "}
      </Grid>
      <Grid item md={4}>
        <h2>Probabilidad</h2>
      </Grid>

      <Grid item md={5.5}>
        <img src={imageBase64} alt="" style={{ waxWidth: 500, height: 200 }} />
      </Grid>
      <Grid item md={6}>
        <img src={image} alt="" style={{ maxWidth: 700, height: 200 }} />
      </Grid>
      <Grid item md={2}>
        {" "}
      </Grid>
      <Grid item md={4} xs={3}>
        <Button variant="outlined" onClick={handleClick}>
          Predecir
        </Button>
      </Grid>
      <Grid item md={2}>
        {" "}
      </Grid>
      <Grid item md={4} xs={3}>
        <Button variant="outlined" onClick={handleClick2}>
          Probabilidad
        </Button>
      </Grid>
    </Grid>
  );
};

export default Menu;

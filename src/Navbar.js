import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";

import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";

import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

const drawerWidth = 240;
const navItems = ["Inicio", "Informes", "Resositorios"];

function NavBar(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: "center" }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        MUI
      </Typography>
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item} disablePadding>
            <ListItemButton sx={{ textAlign: "center" }}>
              <ListItemText primary={item} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar component="nav">
        <Toolbar>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}
          >
            GRUPO 8
          </Typography>
          <Box sx={{ display: { xs: "none", sm: "block" } }}>
            {navItems.map((item) => (
              <Button key={item} sx={{ color: "#fff" }}>
                {item}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </AppBar>
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
      </nav>
      <Box component="main" sx={{ p: 3 }}>
        <Toolbar />
        <Typography>
          Te damos la bienvenida a nuestro proyecto de investigación. El cual se
          centra en la aplicación de modelos de aprendizaje automático para
          abordar el fenómeno de la salud, a través del análisis del conjunto de
          datos “Egresos hospitalarios 2016-2020” proporcionado por el gobierno
          de la provincia de Buenos Aires. Empleando la regresión logística como
          herramienta, relacionamos variables como la edad, el sexo, lugar de
          residencia y patologias del paciente, con los tipos de egresos
          hospitalarios (Alta definitiva, traslado, defunción, entre otros). A
          su vez, exploramos el proceso de clusterización con K-Modes para
          analizar las patologías sufridas en función de la edad. A través de
          estos enfoques, buscamos identificar patrones y relaciones que puedan
          ayudar a predecir si un paciente hospitalizado egresará por alta o
          defunción. Hemos evaluado el desempeño de nuestros modelos utilizando
          métricas como la exactitud, la Curva ROC y los errores cuadráticos.
          Nuestro objetivo es proporcionar una herramienta que facilite la
          selección de datos relacionados con los motivos de internación y, a
          partir de estos, predecir la cantidad de defunciones. Además,
          ofrecemos la posibilidad de ingresar una lista de edades para predecir
          la probabilidad del alta relacionada a estos valores.
        </Typography>
      </Box>
    </Box>
  );
}

NavBar.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default NavBar;
